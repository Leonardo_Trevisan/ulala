library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
    port(
        pcwren              : in std_logic;
        pcclok              : in std_logic;
        pcdata              : in unsigned(9 downto 0);
        pcout               : out unsigned(9 downto 0)
    );
end entity;

architecture arc_PC of PC is
signal reg: unsigned(9 downto 0) := "0000000000";
begin
    process(pcclok, pcwren)
    begin
        if pcwren='1' then
            if rising_edge(pcclok) then
                reg <= pcdata;
            end if;
        end if;
    end process;
    pcout <= reg;
end architecture;