library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Register_16b is
    port( clk           : in std_logic;
        rst             : in std_logic;
        wrenable        : in std_logic;
        datain          : in unsigned(7 downto 0);
        dataout         : out unsigned(7 downto 0)
    );
end entity;

architecture arc_Register of Register_16b is
signal reg: unsigned(7 downto 0) := "00000000";
begin
    process(clk, rst, wrenable)
    begin
        if rst='1' then
            reg <= "00000000";
        elsif wrenable='1' then
            if rising_edge(clk) then
                reg <= datain;
            end if;
        end if;
    end process;
    dataout <= reg;
end architecture;