library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM is
    port( clk      : in std_logic;
          address  : in unsigned(6 downto 0);
          wr_en    : in std_logic;
          data_in  : in unsigned(7 downto 0);
          data_out : out unsigned(7 downto 0)
        );
end entity;

architecture arc_RAM of RAM is 
    type mem is array (0 to 127) of unsigned(7 downto 0);
    signal content : mem;
    begin
        process(clk, wr_en)
        begin
            if rising_edge(clk) then
                if wr_en='1' then 
                content(to_integer(address)) <= data_in;
                end if;
            end if;
        end process;
        data_out <= content(to_integer(address));
end architecture;