library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PCROM_tb is
end;

architecture arc_PCROM_tb of PCROM_tb is
    component PCROM
        port(
            clock                   : in std_logic;
            writeenable             : in std_logic;
            romout                  : out unsigned(13 downto 0)
        );
    end component;
    signal clk, writeenable : std_logic;
    signal pcromout : unsigned(13 downto 0);
begin
    uut: PCROM port map (clock => clk, writeenable => writeenable, romout => pcromout);
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;

    process
    begin 
        writeenable <= '1';
        wait;
    end process;
end architecture;