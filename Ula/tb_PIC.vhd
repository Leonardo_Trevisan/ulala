library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PIC_tb is
end;

architecture arc_PIC_tb of PIC_tb is
    component PIC
        port (
            c_reset                   : in std_logic;
            c_clock                   : in std_logic;
            c_state                   : out unsigned(1 downto 0);
            c_pcout                   : out unsigned(9 downto 0);
            c_romout                  : out unsigned(13 downto 0);
            c_bankreg1                : out unsigned(7 downto 0);
            c_bankreg2                : out unsigned(7 downto 0);
            c_ulaout                  : out unsigned(7 downto 0)
        );
    end component;
signal clock, reset : std_logic;
begin
    uut : PIC port map(c_clock=>clock,c_reset=>reset);
    process
    begin
        clock <= '0';
        wait for 25 ns;
        clock <= '1';
        wait for 25 ns;
    end process;

    process
    begin
        reset <= '1';
        wait for 25 ns;
        reset <= '0';
        wait;
    end process;
end architecture;

