library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ROM_tb is
end;

architecture arc_ROM_tb of ROM_tb is
    component ROM
        port( clk         : in std_logic;
              address       : in unsigned(9 downto 0);
              data          : out unsigned(13 downto 0)
            );
    end component;
signal clk : std_logic;
signal address : unsigned(9 downto 0);
signal data : unsigned(13 downto 0);
begin
    uut: ROM port map (clk=>clk, address=>address, data=>data);
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;

    process
    begin
        wait for 75 ns;
        address <= "0000000000";
        wait for 75 ns;
        address <= "0000000001";
        wait for 75 ns;
        address <= "0000000010";
        wait for 75 ns;
        address <= "0000000011";
        wait for 75 ns;
        address <= "0000000100";
        wait;
    end process;
end;