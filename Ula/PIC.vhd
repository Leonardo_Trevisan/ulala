library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PIC is
    port (
        c_reset                   : in std_logic;
        c_clock                   : in std_logic;
        c_state                   : out unsigned(1 downto 0);
        c_pcout                   : out unsigned(9 downto 0);
        c_romout                  : out unsigned(13 downto 0);
        c_bankreg1                : out unsigned(7 downto 0);
        c_bankreg2                : out unsigned(7 downto 0);
        c_ulaout                  : out unsigned(7 downto 0)
    );
end entity;

architecture arc_PIC of PIC is
    component PICStateMachine
        port( clk                 : in std_logic;
              rst                 : in std_logic;
              state               : out unsigned(1 downto 0)
        );
    end component;
    component PC
        port(   pcwren            : in std_logic;
                pcclok            : in std_logic;
                pcdata            : in unsigned(9 downto 0);
                pcout             : out unsigned(9 downto 0)
            );
    end component;
    component ROM
        port(   clk               : in std_logic;
                address           : in unsigned(9 downto 0);
                data              : out unsigned(13 downto 0)
        );
    end component;
    component RAM
        port(  clk                : in std_logic;
               address            : in unsigned(6 downto 0);
               wr_en              : in std_logic;
               data_in            : in unsigned(7 downto 0);
               data_out           : out unsigned(7 downto 0)
        );
    end component;
    component ULA
        port(   entryA            : in unsigned(7 downto 0);
                entryB            : in unsigned(7 downto 0);
                selop             : in unsigned(1 downto 0);
                zeroout           : out std_logic;
                output            : out unsigned(7 downto 0)
        );
    end component;
    component Bank
        port(   selread1          : in unsigned(2 downto 0);
                selread2          : in unsigned(2 downto 0);
                selwrite          : in unsigned(2 downto 0);
                writeenable       : in std_logic;
                clock             : in std_logic;
                reset             : in std_logic;
                datain            : in unsigned(7 downto 0);
                dataout1          : out unsigned(7 downto 0);
                dataout2          : out unsigned(7 downto 0)
        );
    end component;
    signal sm_state                             : unsigned(1 downto 0);
    signal pc_writeenable                       : std_logic := '0';
    signal pc_nxtaddress, pc_crraddress         : unsigned(9 downto 0) := "0000000000";
    signal rom_out                              : unsigned(13 downto 0);
    signal bank_rdsel1, bank_rdsel2, bank_wrsel : unsigned(2 downto 0);
    signal bank_writeenable                     : std_logic;
    signal bank_reg1, bank_reg2                 : unsigned(7 downto 0);
    signal ula_input1, ula_input2               : unsigned(7 downto 0);
    signal ula_operation                        : unsigned(1 downto 0);
    signal ula_zero                             : std_logic;
    signal ula_output                           : unsigned(7 downto 0);
    signal ram_address                          : unsigned(6 downto 0);
    signal ram_input                            : unsigned(7 downto 0);
    signal ram_output                           : unsigned(7 downto 0);
    signal ram_writeenable                      : std_logic := '0';

    signal op0_2b : unsigned(1 downto 0);
    signal op2_5b : unsigned(2 downto 0);
    signal op2_4b : unsigned(1 downto 0);
    signal op2_6b : unsigned(3 downto 0);
    signal op2_3b : unsigned(0 downto 0);
    signal op2_8b : unsigned(5 downto 0);
    signal op10_12: unsigned(1 downto 0);
    signal f : unsigned(2 downto 0);
    signal d : unsigned(0 downto 0);
    signal b : unsigned(2 downto 0);
    signal f_b0 : unsigned(0 downto 0);
    signal f_b1 : unsigned(0 downto 0);
    signal f_b2 : unsigned(0 downto 0);
    signal f_b3 : unsigned(0 downto 0);
    signal f_b4 : unsigned(0 downto 0);
    signal f_b5 : unsigned(0 downto 0);
    signal f_b6 : unsigned(0 downto 0);
    signal f_b7 : unsigned(0 downto 0);
    signal f_b : unsigned(0 downto 0);
    signal mm : unsigned(1 downto 0);
    signal kaddress : unsigned(9 downto 0);
    begin
        in_machine : PICStateMachine port map(clk => c_clock, rst => c_reset, state => sm_state);
        in_pc : PC port map (pcwren => pc_writeenable, pcclok => c_clock, pcdata => pc_nxtaddress, pcout => pc_crraddress);
        in_rom : ROM port map(clk => c_clock, address => pc_crraddress, data => rom_out);
        in_bank : Bank port map(dataout1 => bank_reg1, dataout2 => bank_reg2, selread1 => bank_rdsel1, selread2 => bank_rdsel2,
                            reset => c_reset, selwrite => bank_wrsel, clock => c_clock, datain => ula_output,
                            writeenable => bank_writeenable);
        in_ula : Ula port map(entryA => ula_input1, entryB => ula_input2, output => ula_output, selop => ula_operation,
                            zeroout => ula_zero);
        in_ram : RAM port map(clk => c_clock, address => ram_address, data_in => ram_input, data_out => ram_output, 
                            wr_en => ram_writeenable);
        
        op0_2b <= rom_out(13 downto 12);
        op2_5b <= rom_out(11 downto 9);
        op2_4b <= rom_out(11 downto 10);
        op2_6b <= rom_out(11 downto 8);
        op2_3b <= rom_out(11 downto 11);
        op2_8b <= rom_out(11 downto 6);
        op10_12<= rom_out(3 downto 2);
        f <= rom_out(2 downto 0);
        d <= rom_out(7 downto 7);
        b <= rom_out(9 downto 7);
        kaddress <= rom_out(9 downto 0);
        f_b0 <= ula_output(0 downto 0);
        f_b1 <= ula_output(1 downto 1);
        f_b2 <= ula_output(2 downto 2);
        f_b3 <= ula_output(3 downto 3);
        f_b4 <= ula_output(4 downto 4);
        f_b5 <= ula_output(5 downto 5);
        f_b6 <= ula_output(6 downto 6);
        f_b7 <= ula_output(7 downto 7);
        f_b  <= f_b0 when b = "000" else
                f_b1 when b = "001" else
                f_b2 when b = "010" else
                f_b3 when b = "011" else
                f_b4 when b = "100" else
                f_b5 when b = "101" else
                f_b6 when b = "110" else
                f_b7 when b = "111" else
                     "0";
        mm <= rom_out(1 downto 0);

        ram_address      <= bank_reg1(6 downto 0) - "0000001" when mm = "10" else --MOVWI/MOVIW mm=10
                            bank_reg1(6 downto 0) + "0000001" when mm = "11" else --MOVWI/MOVIW mm=11
                            bank_reg1(6 downto 0); --MOVWI/MOVIW
        
        ram_input        <= bank_reg2; --MOVWI/MOVIW
        
        ram_writeenable  <= '1' when op0_2b = "11" and op2_6b = "1111" and op10_12 = "10" and  sm_state = "00" else --MOVWI
                            '0';

        bank_wrsel       <= "001" when op0_2b = "11" and op2_6b = "1110" and sm_state = "00" else --ADDLW
                            "001" when op0_2b = "00" and op2_6b = "0001" and d="0" and sm_state = "00" else --CLRW
                            f when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            f when op0_2b = "00" and op2_6b = "0111" and d = "1" and sm_state = "00" else -- ADDWF d=1
                            "001" when op0_2b = "00" and op2_6b = "0111" and d = "0" and sm_state = "00" else -- ADDWF d=0
                            "001" when op0_2b = "11" and (op2_6b = "1101" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            "111" when op0_2b = "11" and op2_8b = "000100" and sm_state = "00" else --ADDFSR
                            "111" when op0_2b = "11" and op2_6b = "1111" and sm_state = "01" else --MOVWI/MOVIW
                            "001" when op0_2b = "11" and op2_6b = "1111" and op10_12 = "00" and sm_state = "00" else --MOVIW
                            "000";

        bank_writeenable <= '1' when op0_2b = "11" and op2_6b = "1110" and sm_state = "00" else--ADDLW
                            '1' when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW
                            '1' when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            '1' when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            '1' when op0_2b = "11" and (op2_6b = "1101" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            '1' when op0_2b = "11" and op2_8b = "000100" and sm_state = "00" else --ADDFSR
                            '1' when op0_2b = "11" and op2_6b = "1111" and sm_state = "01" else --MOVWI/MOVIW
                            '1' when op0_2b = "11" and op2_6b = "1111" and op10_12 = "00" and sm_state = "00" else --MOVIW
                            '0';

        bank_rdsel1      <= "001" when op0_2b = "11" and op2_6b = "1110" and sm_state = "00" else --ADDLW
                            "001" when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "001" when op0_2b = "11" and (op2_6b = "1101" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            f     when op0_2b = "01" and op2_4b = "11" and sm_state = "01" else --BTFSS
                            f     when op0_2b = "01" and op2_4b = "10" and sm_state = "01" else --BTFSC
                            "111" when op0_2b = "11" and op2_8b = "000100" and sm_state = "00" else --ADDFSR
                            "111" when op0_2b = "11" and op2_6b = "1111" else --MOVWI/MOVIW
                            "000";

        bank_rdsel2      <= f when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "001" when op0_2b = "11" and op2_6b = "1111" and op10_12 = "10" and sm_state = "00" else --MOVWI
                            "000";

        ula_input1       <= "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW 
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            "00000000" when op0_2b = "11" and op2_6b = "1111" and op10_12 = "00" and sm_state = "00" else --MOVIW
                            bank_reg1;
    
        ula_input2       <= rom_out(7 downto 0) when op0_2b = "11" and op2_6b = "1110" and sm_state = "00" else --ADDLW
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            bank_reg2 when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF 
                            rom_out(7 downto 0) when op0_2b = "11" and (op2_6b = "1101" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            '0' & '0' & rom_out(5 downto 0) when op0_2b = "11" and op2_8b = "000100" and sm_state = "00" else --ADDFSR
                            "00000001" when op0_2b = "11" and op2_6b = "1111" and sm_state = "01" else --MOVWI
                            ram_output when op0_2b = "11" and op2_6b = "1111" and op10_12 = "00" and sm_state = "00" else --MOVIW
                            "00000000";
        
        ula_operation    <= "00" when op0_2b = "11" and op2_6b = "1110" and sm_state = "00" else --ADDLW
                            "00" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW 
                            "00" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            "00" when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "00" when op0_2b = "01" and op2_4b = "11" and sm_state = "00" else --BTFSS
                            "00" when op0_2b = "11" and op2_8b = "000100" and sm_state = "00" else --ADDFSR
                            "01" when op0_2b = "11" and (op2_6b = "1101" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            "00" when op0_2b = "11" and op2_6b = "1111" and sm_state = "01" and (mm = "00" or mm = "10") else --MOVWI/MOVIW
                            "01" when op0_2b = "11" and op2_6b = "1111" and sm_state = "01" and (mm = "01" or mm = "11") else --MOVWI/MOVIW
                            "00" when op0_2b = "11" and op2_6b = "1111" and op10_12 = "00" and sm_state = "00" else --MOVIW
                            "00";
        
        pc_nxtaddress    <= kaddress when op0_2b = "10" and op2_3b = "1" and sm_state = "01" else --GOTO
                            pc_crraddress+"00000010" when op0_2b = "01" and op2_4b = "11" and f_b = "1" and sm_state = "01" else --BTFSS f(b) = 1
                            pc_crraddress+"00000010" when op0_2b = "01" and op2_4b = "10" and f_b = "0" and sm_state = "01" else --BTFSC f(b) = 0
                            pc_crraddress+"00000001"-rom_out(7 downto 0) when op0_2b = "11" and op2_5b = "001" and rom_out(8 downto 8) = "1" and sm_state = "01" else --BRA k > 0
                            pc_crraddress+"00000001"+rom_out(7 downto 0) when op0_2b = "11" and op2_5b = "001" and rom_out(8 downto 8) = "0" and sm_state = "01" else --BRA k < 0
                            pc_crraddress+"00000001" when sm_state = "01" else --Go to next command
                            pc_crraddress;

        pc_writeenable   <= '1' when sm_state = "01"
                            else '0';

        c_state <= sm_state;
        c_pcout <= pc_nxtaddress;
        c_romout <= rom_out;
        c_bankreg1 <= bank_reg1;
        c_bankreg2 <= bank_reg2;
        c_ulaout <= ula_output;
end architecture;

--ADDLW k  <->  11 1110 kkkk kkkk  <->  Adiciona o literal k ao registrador W (acumulador)
--CLRW  <->  00 0001 0xxx xxxx  <->  zera o registrador W (acumulador)
--CLRF f  <->  00 0001 1fff ffff  <-> zera o registrador f
--ADDWF f,d  <->  00 0111 dfff ffff  <->  Soma W (acumulador) a f e salva em f se d='1' ou W se d='0'
--SUBLW k  <->  11 110x kkkk kkkk  <->  Subtrai o literal k ao registrador W (acumulador)
--GOTO k  <->  10 1kkk kkkk kkkk  <->  Pula para o endereço k
--BCF f,b  <->  01 00bb bfff ffff  <->  Limpa o b-ésimo bit do registrador f como 0
--BSF f,b  <->  01 01bb bfff ffff  <->  Define o b-ésimo bit do registrador f como 1
--BTFSC f,b  <->  01 10bb bfff ffff  <->  Pula proximo comando se o b-ésimo bit do registrador for 0
--BTFSS f,b  <->  01 11bb bfff ffff  <->  Pula proximo comando se o b-ésimo bit do registrador for 1
--BRA k  <->  11 001k kkkk kkkk  <->  Pula para o endereço PC + 1 + k
--ADDFSR k  <->  11 0001 00kk kkkk  <->  Adiciona o literal k ao registrador FRS0
--MOVWI  <->  11 1111 0000 10mm  <->  Move registrador W (acumulador) para a RAM no endereço FRS0
--MOVIW  <->  11 1111 0000 00mm  <->  Move o endereço FRS0 da RAM para o registrador W (acumulador)

--Adaptações:
--O registrador FRS0 ficou como o registrador 7
--O registrador W (acumulador) ficou como o registrador 1
--O número de registradores está temporariamente reduzido