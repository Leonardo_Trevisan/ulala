library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FlipFlopT is
    port( ffclock          : in std_logic;
          ffreset          : in std_logic;
          ffout            : out std_logic
        );
end entity;

architecture arc_FlipFlopT of FlipFlopT is
    signal fflast : std_logic := '0';
    begin
    process(ffclock, ffreset)
    begin
        if ffreset = '1' then
            ffout <= '0';
        elsif rising_edge(ffclock) then
            fflast <= not (fflast);
            ffout <= fflast;
        end if;
    end process;
end architecture;