library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC_tb is
end;

architecture arc_PC_tb of PC_tb is
    component PC
        port(
            pcwren              : in std_logic;
            pcclok              : in std_logic;
            pcdata              : in unsigned(9 downto 0);
            pcout               : out unsigned(9 downto 0)
        );
    end component;
    component PlusOne
        port(
            inplus              : in unsigned(9 downto 0);
            outplus             : out unsigned(9 downto 0)
        );
    end component;
    signal pcwren, pcclok: std_logic;
    signal sg_pcout, sg_plusout : unsigned(9 downto 0);
    begin
        plon : PlusOne port map(inplus=>sg_pcout,outplus=>sg_plusout);
        uut: PC port map (pcwren => pcwren, pcclok => pcclok, pcdata => sg_plusout, pcout => sg_pcout);
        process
        begin
            pcclok <= '0';
            wait for 50 ns;
            pcclok <= '1';
            wait for 50 ns;
        end process;

        process
        begin
            pcwren <= '1';
            wait;
        end process;
end architecture;