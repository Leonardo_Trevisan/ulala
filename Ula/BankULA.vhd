library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BankULA is
    port(
        extconst                 : in unsigned(15 downto 0);
        wrtenb                   : in std_logic;
        clk                      : in std_logic;
        rst                      : in std_logic;
        muxsel                   : in std_logic;
        selectop                 : in unsigned(1 downto 0);
        selectread1              : in unsigned(2 downto 0);
        selectread2              : in unsigned(2 downto 0);
        selectwrite              : in unsigned(2 downto 0);
        zerout                   : out std_logic;
        uladebug                 : out unsigned(15 downto 0)
    );
end entity;

architecture arc_BankULA of BankULA is
    component Bank
        port(   selread1          : in unsigned(2 downto 0);
                selread2          : in unsigned(2 downto 0);
                selwrite          : in unsigned(2 downto 0);
                writeenable       : in std_logic;
                clock             : in std_logic;
                reset             : in std_logic;
                datain            : in unsigned(15 downto 0);
                dataout1          : out unsigned(15 downto 0);
                dataout2          : out unsigned(15 downto 0)
    );
    end component;
    component ULA
        port( entryA            : in unsigned(15 downto 0);
              entryB            : in unsigned(15 downto 0);
              selop             : in unsigned(1 downto 0);
              zeroout           : out std_logic;
              output            : out unsigned(15 downto 0)
    );
    end component;
    signal ulaout: unsigned(15 downto 0);
    signal ulaentryA: unsigned(15 downto 0);
    signal ulaentryB: unsigned(15 downto 0);
    signal bankin: unsigned(15 downto 0);
    signal bankoutA: unsigned(15 downto 0);
    signal bankoutB: unsigned(15 downto 0);
begin
    cbank : Bank port map(selread1=>selectread1,
                          selread2=>selectread2,
                          selwrite=>selectwrite,
                          writeenable=>wrtenb,
                          clock=>clk,
                          reset=>rst,
                          datain=>bankin,
                          dataout1=>bankoutA,
                          dataout2=>bankoutB);
    cula : ULA port map  (entryA=>ulaentryA,
                          entryB=>ulaentryB,
                          selop=>selectop,
                          zeroout=>zerout,
                          output=> ulaout);
                
    bankin <= ulaout;
    uladebug <= ulaout;
    ulaentryA <= bankoutA;
    ulaentryB <= bankoutB when muxsel='1' else
                     extconst when muxsel='0' else
                     "0000000000000000";
end architecture;