library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC is
    port( clock : in std_logic;
          reset : in std_logic 
        );
end entity;

architecture arc_UC of UC is
    component ROM
        port( clk               : in std_logic;
              endress           : in unsigned(7 downto 0);
              data              : out unsigned(13 downto 0)
            );
    end component;
    component PC
        port( pcwren              : in std_logic;
              pcclok              : in std_logic;
              pcdata              : in unsigned(7 downto 0);
              pcout               : out unsigned(7 downto 0)
            );
    end component;
    component FlipFlopT
        port( ffclock          : in std_logic;
              ffreset          : in std_logic;
              ffout            : out std_logic
            );
    end component;
    signal wrtenb : std_logic := '1';
    signal statemch : std_logic;
    signal romout : unsigned(13 downto 0);
    signal sg_crrendress : unsigned(7 downto 0);
    signal sg_nxtendress : unsigned(7 downto 0) := "00000000";
    signal opcode : unsigned(3 downto 0);
    begin
        fft : FlipFlopT port map(ffclock => clock, ffreset => reset, ffout => statemch);
        ins_pc: PC port map (pcwren => wrtenb, pcclok => statemch, pcdata => sg_nxtendress, pcout => sg_crrendress);
        ins_rom : ROM port map(endress => sg_crrendress, data => romout, clk => clock);
        opcode <= romout(13 downto 10);
        sg_nxtendress <= romout(7 downto 0) when opcode ="1111" else sg_crrendress + "00000001";
end architecture;