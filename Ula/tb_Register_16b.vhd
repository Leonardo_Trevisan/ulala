library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Register_16b_tb is
end;

architecture arc_Register_16b_tb of Register_16b_tb is
    component Register_16b
        port( clk             : in std_logic;
              rst             : in std_logic;
              wrenable        : in std_logic;
              datain          : in unsigned(7 downto 0);
              dataout         : out unsigned(7 downto 0)
    );
    end component;
    signal clk, rst, wrenable: std_logic;
    signal datain: unsigned(7 downto 0);
    signal dataout: unsigned(7 downto 0);
    begin
        uut: Register_16b port map (clk=>clk, rst=>rst, wrenable=>wrenable, datain=>datain, dataout=>dataout);
        process
        begin
            clk <= '0';
            wait for 50 ns;
            clk <= '1';
            wait for 50 ns;
        end process;
        process
        begin
            rst <= '1';
            wait for 100 ns;
            rst <= '0';
            wait;
        end process;

        process
        begin
            wait for 100 ns;
            wrenable <= '0';
            datain <= "11111111";
            wait for 100 ns;
            datain <= "10101010";
            wait for 100 ns;
            wrenable <= '1';
            datain <= "10101010";
            wait;
        end process;
end architecture;