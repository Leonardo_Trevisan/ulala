library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Bank is
    port(   selread1          : in unsigned(2 downto 0);
            selread2          : in unsigned(2 downto 0);
            selwrite          : in unsigned(2 downto 0);
            writeenable       : in std_logic;
            clock             : in std_logic;
            reset             : in std_logic;
            datain            : in unsigned(7 downto 0);
            dataout1          : out unsigned(7 downto 0);
            dataout2          : out unsigned(7 downto 0)
    );
end entity;

architecture arc_Bank of Bank is
    component Register_16b
        port( clk             : in std_logic;
              rst             : in std_logic;
              wrenable        : in std_logic;
              datain          : in unsigned(7 downto 0);
              dataout         : out unsigned(7 downto 0)
        );
    end component;
    signal regout1 : unsigned(7 downto 0);
    signal wrten1  : std_logic;
    signal regout2 : unsigned(7 downto 0);
    signal wrten2  : std_logic;
    signal regout3 : unsigned(7 downto 0);
    signal wrten3  : std_logic;
    signal regout4 : unsigned(7 downto 0);
    signal wrten4  : std_logic;
    signal regout5 : unsigned(7 downto 0);
    signal wrten5  : std_logic;
    signal regout6 : unsigned(7 downto 0);
    signal wrten6  : std_logic;
    signal regout7 : unsigned(7 downto 0);
    signal wrten7  : std_logic;
begin
    register1: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten1,
                                      dataout => regout1,
                                      datain => datain);
    register2: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten2,
                                      dataout => regout2,
                                      datain => datain);
    register3: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten3,
                                      dataout => regout3,
                                      datain => datain);
    register4: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten4,
                                      dataout => regout4,
                                      datain => datain);
    register5: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten5,
                                      dataout => regout5,
                                      datain => datain);
    register6: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten6,
                                      dataout => regout6,
                                      datain => datain);
    register7: Register_16b port map (clk => clock,
                                      rst => reset,
                                      wrenable => wrten7,
                                      dataout => regout7,
                                      datain => datain);
    
    wrten1 <= '1' when selwrite="001" and writeenable='1' else 
              '0';
    wrten2 <= '1' when selwrite="010" and writeenable='1' else '0';
    wrten3 <= '1' when selwrite="011" and writeenable='1' else '0';
    wrten4 <= '1' when selwrite="100" and writeenable='1' else '0';
    wrten5 <= '1' when selwrite="101" and writeenable='1' else '0';
    wrten6 <= '1' when selwrite="110" and writeenable='1' else '0';
    wrten7 <= '1' when selwrite="111" and writeenable='1' else '0';

    dataout1 <= "00000000" when selread1="000" else
                regout1 when selread1="001" else
                regout2 when selread1="010" else
                regout3 when selread1="011" else
                regout4 when selread1="100" else
                regout5 when selread1="101" else
                regout6 when selread1="110" else
                regout7 when selread1="111" else
                "00000000";

    dataout2 <= "00000000" when selread2="000" else
                regout1 when selread2="001" else
                regout2 when selread2="010" else
                regout3 when selread2="011" else
                regout4 when selread2="100" else
                regout5 when selread2="101" else
                regout6 when selread2="110" else
                regout7 when selread2="111" else
                "00000000";
end architecture;