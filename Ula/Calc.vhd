library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Calc is
    port (
        c_reset                   : in std_logic;
        c_clock                   : in std_logic;
        c_state                   : out unsigned(1 downto 0);
        c_pcout                   : out unsigned(9 downto 0);
        c_romout                  : out unsigned(13 downto 0);
        c_bankreg1                : out unsigned(7 downto 0);
        c_bankreg2                : out unsigned(7 downto 0);
        c_ulaout                  : out unsigned(7 downto 0)
    );
end entity;

architecture arc_Calc of Calc is
    component CalcStateMachine
        port( clk                 : in std_logic;
              rst                 : in std_logic;
              state               : out unsigned(1 downto 0)
        );
    end component;
    component PC
        port(   pcwren            : in std_logic;
                pcclok            : in std_logic;
                pcdata            : in unsigned(9 downto 0);
                pcout             : out unsigned(9 downto 0)
            );
    end component;
    component ROM
        port(   clk               : in std_logic;
                address           : in unsigned(9 downto 0);
                data              : out unsigned(13 downto 0)
        );
    end component;
    component ULA
        port(   entryA            : in unsigned(7 downto 0);
                entryB            : in unsigned(7 downto 0);
                selop             : in unsigned(1 downto 0);
                zeroout           : out std_logic;
                output            : out unsigned(7 downto 0)
        );
    end component;
    component Bank
        port(   selread1          : in unsigned(2 downto 0);
                selread2          : in unsigned(2 downto 0);
                selwrite          : in unsigned(2 downto 0);
                writeenable       : in std_logic;
                clock             : in std_logic;
                reset             : in std_logic;
                datain            : in unsigned(7 downto 0);
                dataout1          : out unsigned(7 downto 0);
                dataout2          : out unsigned(7 downto 0)
        );
    end component;
    signal sm_state                             : unsigned(1 downto 0);
    signal pc_writeenable                       : std_logic := '0';
    signal pc_nxtaddress, pc_crraddress         : unsigned(9 downto 0) := "0000000000";
    signal rom_out                              : unsigned(13 downto 0);
    signal bank_rdsel1, bank_rdsel2, bank_wrsel : unsigned(2 downto 0);
    signal bank_writeenable                     : std_logic;
    signal bank_reg1, bank_reg2                 : unsigned(7 downto 0);
    signal ula_input1, ula_input2               : unsigned(7 downto 0);
    signal ula_operation                        : unsigned(1 downto 0);
    signal ula_zero                             : std_logic;
    signal ula_output                           : unsigned(7 downto 0);

    signal op0_2b : unsigned(1 downto 0);
    signal op2_6b : unsigned(3 downto 0);
    signal op2_3b : unsigned(0 downto 0);
    signal f : unsigned(2 downto 0);
    signal d : unsigned(0 downto 0);
    signal kaddress : unsigned(9 downto 0);
    begin
        in_machine : CalcStateMachine port map(clk => c_clock, rst => c_reset, state => sm_state);
        in_pc : PC port map (pcwren => pc_writeenable, pcclok => c_clock, pcdata => pc_nxtaddress, pcout => pc_crraddress);
        in_rom : ROM port map(clk => c_clock, address => pc_crraddress, data => rom_out);
        in_bank : Bank port map(dataout1 => bank_reg1, dataout2 => bank_reg2, selread1 => bank_rdsel1, selread2 => bank_rdsel2,
                            reset => c_reset, selwrite => bank_wrsel, clock => c_clock, datain => ula_output,
                            writeenable => bank_writeenable);
        in_ula : Ula port map(entryA => ula_input1, entryB => ula_input2, output => ula_output, selop => ula_operation,
                              zeroout => ula_zero);
        
        op0_2b <= rom_out(13 downto 12);
        op2_6b <= rom_out(11 downto 8);
        op2_3b <= rom_out(11 downto 11);
        f <= rom_out(2 downto 0);
        d <= rom_out(7 downto 7);
        kaddress <= rom_out(9 downto 0);

        bank_wrsel       <= "001" when op0_2b = "11" and (op2_6b = "1111" or op2_6b = "1110") and sm_state = "00" else --ADDLW
                            "001" when op0_2b = "00" and op2_6b = "0001" and d="0" and sm_state = "00" else --CLRW
                            f when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            f when op0_2b = "00" and op2_6b = "0111" and d = "1" and sm_state = "00" else -- ADDWF d=1
                            "001" when op0_2b = "00" and op2_6b = "0111" and d = "0" and sm_state = "00" else -- ADDWF d=0
                            "001" when op0_2b = "11" and (op2_6b = "1100" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            "000";

        bank_writeenable <= '1' when op0_2b = "11" and (op2_6b = "1111" or op2_6b = "1110") and sm_state = "00" else--ADDLW
                            '1' when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW
                            '1' when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            '1' when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            '1' when op0_2b = "11" and (op2_6b = "1100" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            '0';

        bank_rdsel1      <= "001" when op0_2b = "11" and (op2_6b = "1111" or op2_6b = "1110") and sm_state = "00" else --ADDLW
                            "001" when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "001" when op0_2b = "11" and (op2_6b = "1100" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            "000";

        bank_rdsel2      <= f when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "000";

        ula_input1       <= "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW 
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            bank_reg1;
    
        ula_input2       <= rom_out(7 downto 0) when op0_2b = "11" and (op2_6b = "1111" or op2_6b = "1110") and sm_state = "00" else --ADDLW
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW
                            "00000000" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            bank_reg2 when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF 
                            rom_out(7 downto 0) when op0_2b = "11" and (op2_6b = "1100" or op2_6b = "1100") and sm_state = "00" else --SUBLW
                            "00000000";
        
        ula_operation    <= "00" when op0_2b = "11" and (op2_6b = "1111" or op2_6b = "1110") and sm_state = "00" else --ADDLW
                            "00" when op0_2b = "00" and op2_6b = "0001" and d = "0" and sm_state = "00" else --CLRW 
                            "00" when op0_2b = "00" and op2_6b = "0001" and d = "1" and sm_state = "00" else --CLRF
                            "00" when op0_2b = "00" and op2_6b = "0111" and sm_state = "00" else -- ADDWF
                            "01" when op0_2b = "11" and (op2_6b = "1100" or op2_6b = "1100") and sm_state = "00" else
                            "00";
        
        pc_nxtaddress    <= kaddress when op0_2b = "10" and op2_3b = "1" and sm_state = "01" else --GOTO
                            pc_crraddress+"00000001" when sm_state = "01" else
                            pc_crraddress;

        pc_writeenable   <= '1' when sm_state = "01"
                            else '0';

        c_state <= sm_state;
        c_pcout <= pc_nxtaddress;
        c_romout <= rom_out;
        c_bankreg1 <= bank_reg1;
        c_bankreg2 <= bank_reg2;
        c_ulaout <= ula_output;
end architecture;

--ADDLW k  <->  11 111x kkkk kkkk  <->  Adiciona o literal k ao registrador W (acumulador)
--CLRW  <->  00 0001 0xxx xxxx  <->  zera o registrador W (acumulador)
--CLRF f  <->  00 0001 1fff ffff  <-> zera o registrador f
--ADDWF f,d  <->  00 0111 dfff ffff  <->  Soma W (acumulador) a f e salva em f se d='1' ou W se d='0'
--SUBLW k  <->  11 110x kkkk kkkk  <->  Subtrai o literal k ao registrador W (acumulador)
--GOTO k  <->  10 1kkk kkkk kkkk <-> Pula para o endereço k

--Adaptações:
--O registardor W (acumulador) ficou como o registrador 1
--O número de registradores está temporariamente reduzido