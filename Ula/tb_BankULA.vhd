library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BankULA_tb is
end;

architecture arc_BankULA_tb of BankULA_tb is
    component BankULA
        port(
            extconst                 : in unsigned(15 downto 0);
            wrtenb                   : in std_logic;
            clk                      : in std_logic;
            rst                      : in std_logic;
            muxsel                   : in std_logic;
            selectop                 : in unsigned(1 downto 0);
            selectread1              : in unsigned(2 downto 0);
            selectread2              : in unsigned(2 downto 0);
            selectwrite              : in unsigned(2 downto 0);
            zerout                   : out std_logic;
            uladebug                 : out unsigned(15 downto 0)
        );
    end component;
signal selectwrite, selectread1, selectread2: unsigned(2 downto 0);
signal zerout, wrtenb, clk, rst, muxsel: std_logic;
signal selectop: unsigned(1 downto 0);
signal extconst, uladebug : unsigned(15 downto 0);
begin
    uut: BankULA port map(extconst=>extconst,
                          wrtenb=>wrtenb,
                          clk=>clk,
                          rst=>rst,
                          muxsel=>muxsel,
                          selectop=>selectop,
                          selectread1=>selectread1,
                          selectread2=>selectread2,
                          selectwrite=>selectwrite,
                          zerout=>zerout,
                          uladebug=>uladebug);
    process
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;

    process
    begin
        rst <= '1';
        wait for 100 ns;

        rst <= '0';
        wait for 100 ns;

        muxsel <= '0';
        extconst <= "0000000000000111";
        selectread1 <= "000";
        wrtenb <= '1';
        selectwrite <= "001";
        selectop <= "00";
        wait for 100 ns;

        muxsel <= '0';
        extconst <= "0000000000001111";
        selectread1 <= "001";
        wrtenb <= '1';
        selectwrite <= "010";
        selectop <= "00";
        wait for 100 ns;

        muxsel <= '1';
        extconst <= "0000000000000000";
        selectread1 <= "001";
        selectread2 <= "010";
        wrtenb <= '0';
        selectwrite <= "011";
        selectop <= "00";
        
        wait;
    end process;
end architecture;