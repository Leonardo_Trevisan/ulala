library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ULA_tb is
end;

architecture arc_ULA_tb of ULA_tb is
    component ULA
        port( entryA            : in unsigned(7 downto 0);
              entryB            : in unsigned(7 downto 0);
              selop             : in unsigned(1 downto 0);
              zeroout           : out std_logic;
              output            : out unsigned(7 downto 0)
    );
    end component;
signal entryA, entryB: unsigned(7 downto 0);
signal zeroout: std_logic;
signal output: unsigned(7 downto 0);
signal selop: unsigned(1 downto 0);
begin
    uut: ULA port map( entryA => entryA,
                       entryB => entryB,
                       selop => selop,
                       zeroout => zeroout,
                       output => output);
    process
    begin
        entryA <= "00100011"; --35
        entryB <= "00000111"; --7
        selop <= "00";
        wait for 50 ns;

        selop <= "01";
        wait for 50 ns;
        
        selop <= "10";
        wait for 50 ns;
        
        entryA <= "00100011"; --35
        entryB <= "00100011"; --35
        selop <= "11";
        wait for 50 ns;
        wait;
    end process;
end architecture;