    library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Bank_tb is
end;

architecture arc_Bank_tb of Bank_tb is
    component Bank
        port(   selread1          : in unsigned(2 downto 0);
                selread2          : in unsigned(2 downto 0);
                selwrite          : in unsigned(2 downto 0);
                writeenable       : in std_logic;
                clock             : in std_logic;
                reset             : in std_logic;
                datain            : in unsigned(7 downto 0);
                dataout1          : out unsigned(7 downto 0);
                dataout2          : out unsigned(7 downto 0)
    );
    end component;
signal selread1, selread2, selwrite: unsigned(2 downto 0);
signal writeenable, clock, reset: std_logic;
signal dataout1, dataout2, datain: unsigned(7 downto 0);
begin
    uut: Bank port map (selread1=>selread1,
                        selread2=>selread2,
                        selwrite=>selwrite,
                        writeenable=>writeenable,
                        clock=>clock,
                        reset=>reset,
                        dataout1=>dataout1,
                        dataout2=>dataout2,
                        datain=>datain);

    process
    begin
        clock <= '0';
        wait for 50 ns;
        clock <= '1';
        wait for 50 ns;
    end process;

    process
    begin
        reset <= '1';
        wait for 100 ns;
        reset <= '0';
        selwrite <= "010";
        selread1 <= "010";
        selread2 <= "000";
        datain <= "00001111";
        wait for 100 ns;
        writeenable <= '1';
        wait for 100 ns;
        writeenable <= '0';
        datain <= "00001001";
        wait for 100 ns;
        reset <= '1';
        wait;
    end process;
end architecture;