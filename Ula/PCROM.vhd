library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PCROM is
    port(
        clock                   : in std_logic;
        writeenable             : in std_logic;
        romout                  : out unsigned(13 downto 0)
    );
end entity;

architecture arc_PCROM of PCROM is
    component ROM
        port( clk               : in std_logic;
              endress           : in unsigned(7 downto 0);
              data              : out unsigned(13 downto 0)
            );
    end component;
    component PC
        port(
            pcwren              : in std_logic;
            pcclok              : in std_logic;
            pcdata              : in unsigned(7 downto 0);
            pcout               : out unsigned(7 downto 0)
        );
    end component;
    component PlusOne
        port(
            inplus              : in unsigned(7 downto 0);
            outplus             : out unsigned(7 downto 0)
        );
    end component;
    signal sg_pcout, sg_plusout : unsigned(7 downto 0);
begin
    ins_pc: PC port map (pcwren => writeenable, pcclok => clock, pcdata => sg_plusout, pcout => sg_pcout);
    ins_plon : PlusOne port map(inplus=>sg_pcout,outplus=>sg_plusout);
    ins_rom : ROM port map(endress => sg_pcout, data => romout, clk => clock);
end architecture;