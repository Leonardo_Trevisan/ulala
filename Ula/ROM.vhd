library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ROM is
    port( clk           : in std_logic;
          address       : in unsigned(9 downto 0);
          data          : out unsigned(13 downto 0)
        );
end entity;

architecture arc_ROM of ROM is
    type memory is array (0 to 255) of unsigned(13 downto 0);
    constant content : memory := (
		0 => "11111000000001",
		1 => "11000100000001",
		2 => "11111000000001",
		3 => "11111100001000",
		4 => "01111010000001",
		5 => "11001100000100",
		6 => "00000110000111",
		7 => "00000100000000",
		8 => "11111000000010",
		9 => "00011110000010",
		10 => "00000100000000",
		11 => "00000110000011",
		12 => "00000100000000",
		13 => "00011100000010",
		14 => "00011110000011",
		15 => "00000100000000",
		16 => "00011100000011",
		17 => "00000110000111",
		18 => "00011110000111",
		19 => "00000100000000",
		20 => "11111100001010",
		21 => "00011100000011",
		22 => "11110000100000",
		23 => "01101110000001",
		24 => "11001100001101",
		25 => "00000110000111",
		26 => "00000100000000",
		27 => "00011100000010",
		28 => "00011110000111",
		29 => "00000100000000",
		30 => "11111100000010",
		31 => "11110000000001",
		32 => "01101110000001",
		33 => "11001100000101",
		34 => "11111000000001",
		35 => "00000110000010",
		36 => "00011110000010",
		37 => "00000100000000",
		38 => "00011100000010",
		39 => "11110000100000",
		40 => "01101110000001",
		41 => "11001100100000",

        others => ( others => '0')
        );
begin
    process(clk)
    begin
        if (rising_edge(clk)) then
            data <= content(to_integer(address));
        end if;
    end process;
end architecture;
              
              

--ADDLW k  <->  11 1110 kkkk kkkk  <->  Adiciona o literal k ao registrador W (acumulador)
--CLRW  <->  00 0001 0xxx xxxx  <->  zera o registrador W (acumulador)
--CLRF f  <->  00 0001 1fff ffff  <-> zera o registrador f
--ADDWF f,d  <->  00 0111 dfff ffff  <->  Soma W (acumulador) a f e salva em f se d='1' ou W se d='0'
--SUBLW k  <->  11 110x kkkk kkkk  <->  Subtrai o literal k ao registrador W (acumulador)
--GOTO k  <->  10 1kkk kkkk kkkk <-> Pula para o endereço k
--BTFSC f,b  <->  01 10bb bfff ffff  <->  Pula proximo comando se o b-ésimo bit do registrador for 0
--BTFSS f,b  <->  01 11bb bfff ffff  <->  Pula proximo comando se o b-ésimo bit do registrador for 1
--BRA k  <->  11 001k kkkk kkkk  <->  Pula para o endereço PC + 1 + k
--ADDFSR k  <->  11 0001 00kk kkkk  <->  Adiciona o literal k ao registrador FRS0
--MOVWI  <->  11 1111 0000 10mm  <->  Move registrador W (acumulador) para a RAM no endereço FRS0
--MOVIW  <->  11 1111 0000 00mm  <->  Move o endereço FRS0 da RAM para o registrador W (acumulador)

--Adaptações:
--O registrador FRS0 ficou como o registrador 7
--O registardor W (acumulador) ficou como o registrador 1
--O número de registradores está temporariamente reduzido