library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FlipFlopT_tb is
end;

architecture arc_FlipFlopT_tb of FlipFlopT_tb is
    component FlipFlopT
        port ( ffclock          : in std_logic;
               ffreset          : in std_logic;
               ffout            : out std_logic
            );
    end component;
signal ffclock, ffreset, ffout : std_logic;
begin
    uut: FlipFlopT port map (ffclock => ffclock, ffreset => ffreset, ffout => ffout);
    process
    begin
        ffclock <= '0';
        wait for 50 ns;
        ffclock <= '1';
        wait for 50 ns;
    end process;

    process
    begin
        ffreset <= '1';
        wait for 100 ns;
        ffreset <= '0';
        wait;
    end process;
end architecture;