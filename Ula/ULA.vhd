library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ULA is
    port( entryA : in unsigned(7 downto 0);
          entryB : in unsigned(7 downto 0);
          selop  : in unsigned(1 downto 0);
          zeroout: out std_logic;
          output : out unsigned(7 downto 0)
        );
end entity;

architecture arc_ULA of ULA is
begin
    output <=   entryA+entryB when selop="00" else
                entryA-entryB when selop="01" else
                entryA/entryB when selop="10" else
                "00000000";
    zeroout<=   '1' when selop="11" and entryA = entryB else
                '0';
end architecture;