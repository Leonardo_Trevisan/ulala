library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PlusOne_tb is
end;

architecture arc_PlusOne_tb of PlusOne_tb is
    component PlusOne
        port(
            inplus      : in unsigned(9 downto 0);
            outplus     : out unsigned(9 downto 0)
        );
    end component;
signal uttinplus, uttoutplus : unsigned(9 downto 0);
begin
    uut: PlusOne port map(inplus => uttinplus, outplus => uttoutplus);
    process
    begin
        uttinplus <= "0000000001";
        wait for 50 ns;
        uttinplus <= "0000000010";
        wait for 50 ns;
        uttinplus <= "0000000011";
        wait for 50 ns;
        uttinplus <= "0000000100";
        wait;
    end process;
end architecture;