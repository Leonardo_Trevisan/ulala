library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PlusOne is
    port(
        inplus      : in unsigned(9 downto 0);
        outplus     : out unsigned(9 downto 0)
    );
end entity;

architecture arc_PlusOne of PlusOne is
    begin
        outplus <= inplus + "0000000001";
end architecture;