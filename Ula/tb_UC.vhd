library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC_tb is
end;

architecture arc_UC_tb of UC_tb is
    component UC
        port( clock : in std_logic;
              reset : in std_logic 
            );
    end component;
signal clock, reset : std_logic;
begin
    uut : UC port map(clock=>clock,reset=>reset);
    process
    begin
        clock <= '0';
        wait for 50 ns;
        clock <= '1';
        wait for 50 ns;
    end process;
end architecture;

